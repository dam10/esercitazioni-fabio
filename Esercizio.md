### Esercitazione GIT + Test + MR

Clonare https://gitlab.com/medi-it/esercitazioni

Per ogni esercizio che si vuole risolvere

1. Scegliere l'esercizio da risolvere   
2. Creare un branch locale basato su `master` dedicato alla soluzione (es. `napoleoni-fattorizzazione`)
3. Creare un package personale dedicato alla soluzione (es. `it.corsojava.ex01.napoleoni`)
4. Nel package creato andare ad implementare la propria soluzione (es. `it.corsojava.ex01.napoleoni.SoluzioneFattoriale`)
5. Nella struttura di test creare un proprio test privato (es. `it.corsojava.ex01.napoleoni.SoluzioneFattoriale`)   
6. Risolvere l'esercizio (e verificarne la soluzione utilizzando il test creato per la propria classe)
7. Commit + Push del branch + Merge Reqeust per la soluzione proposta

Iterare i passi descritti per ogni esercizio di cui si vuole tentare la risoluzione.
