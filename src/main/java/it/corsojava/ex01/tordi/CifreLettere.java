package it.corsojava.ex01.tordi;

/**
 * @author Fabio Napoleoni
 */
public class CifreLettere {
    public static void main(String[] args) {
        cifreToLettere(2127);
        cifreToLettere(3621);
    }

    /**
     * Scrivere un programma che legge un intero n e stampa le cifre di n in lettere.
     * Ad esempio, se n = 2127, il programma stampa esattamente: due uno due sette.
     * @param n il numero da scomporre in cifre
     */
    public static void cifreToLettere(int n) {
        String stringaNumero = "";
        int a = n;
        String[] caratteri = {"zero","uno","due","tre","quattro","cinque","sei","sette","otto","nove"};
        String numConv = String.valueOf(a);

        for(int i = 0; i < numConv.length();i++){
            stringaNumero = stringaNumero + " "+ caratteri[Integer.parseInt(String.valueOf(numConv.charAt(i)))];
        }
        System.out.println(stringaNumero);
    }
}
