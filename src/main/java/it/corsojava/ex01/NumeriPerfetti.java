package it.corsojava.ex01;

/**
 * @author Fabio Napoleoni
 */
public class NumeriPerfetti {
    public static void main(String[] args) {
        numeriPerfetti(6);
        numeriPerfetti(1000);
    }

    /**
     * Un numero perfetto è un numero intero che è uguale alla somma dei suoi divisori propri,
     * ad esempio 6 è perfetto perché 6 = 1 + 2 + 3, mentre 8 non è perfetto dato che 1 + 2 + 4 non fa 8.
     * Scrivere un programma che letto un intero M stampa tutti i numeri perfetti minori od uguali a M
     * e le relative somme dei divisori. Ad esempio se M = 1000 il programma stamperà:
     *
     * 6 = 1 + 2 + 3
     * 28 = 1 + 2 + 4 + 7 + 14
     * 496 = 1 + 2 + 4 + 8 + 16 + 31 + 62 + 124 + 248
     */
    public static void numeriPerfetti(int n) {
        // Completare qui il programma usando la variabile in input n
    }
}
