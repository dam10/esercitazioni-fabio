package it.corsojava.ex01.villa.monete;

/**
 * @author Fabio Napoleoni
 */
public class Monete {
    public static void main(String[] args) {
        monete(97);
    }

    /**
     * Scrivere un programma che letto un numero intero rappresentante un importo in centesimi di euro stampa
     * le monete da 50, 20, 10, 5, 2 e 1 centesimi di euro che servono per fare l'importo.
     * Ad esempio, se l'importo è di 97 centesimi allora il programma stampa:
     *
     * 1 moneta da 50
     * 2 monete da 20
     * 1 moneta da 5
     * 1 moneta da 2
     *
     * @param n il numero di centesimi da stampare
     */
    public static void monete(int n) {
        // Completare qui il programma usando la variabile in input n
        n = 97;


        int c50 = 0;
        int c20 = 0;
        int c10 = 0;
        int c5 = 0;
        int c2 = 0;
        int c1 = 0;


        while (n > 0) {

            if ((n / 50) > 0) {
                n = (n - 50);
                c50++;

            } else if ((n / 20) > 0) {
                n = (n - 20);
                c20++;
            } else if ((n / 10) > 0) {
                n = n - 10;
                c10++;
            } else if ((n / 5) > 0) {
                n = n - 5;
                c5++;
            } else if ((n / 2) > 0) {
                n = n - 2;
                c2++;
            } else if ((n) > 0) {
                n = n - 1;
                c1++;
            } else {
                System.out.println("n =" + n);
            }


        }

        if (c50 > 0) {
            System.out.println(c50 + " " + controllaStringa(c50) + " da 50");
        }
        if (c20 > 0) {
            System.out.println(c20 + " " + controllaStringa(c20) + " da 20");
        }
        if (c10 > 0) {
            System.out.println(c10 + " " + controllaStringa(c10) + " da 10");
        }
        if (c5 > 0) {
            System.out.println(c5 + " " + controllaStringa(c5) + " da 5");
        }
        if (c2 > 0) {
            System.out.println(c2 + " " + controllaStringa(c2) + " da 2");
        }
        if (c1 > 0) {
            System.out.println(c1 + " " + controllaStringa(c1) + " da 1");
        }
    }


    private static String controllaStringa(int contatore){
        String result;
        if (contatore ==1) {
            result = "moneta";
        } else {
            result = "monete";
        }
        return result;
    }
}
