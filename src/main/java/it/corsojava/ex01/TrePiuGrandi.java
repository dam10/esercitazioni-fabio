package it.corsojava.ex01;

import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class TrePiuGrandi {

    /**
     * Scrivere un programma che legge una serie di numeri interi positivi (la lettura si interrompe quando è letto
     * un numero negativo) e stampa i tre numeri più grandi della serie.
     * Ad esempio, se la serie di numeri è 2 10 8 7 1 12 2 -1 allora il programma stampa:
     *
     * I tre numeri più grandi sono: 12, 10, 8
     *
     * N.B. non si possono usare array in questo esercizio!
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        while (in.hasNextInt() && (n = in.nextInt()) > 0) {
            // ...
        }
    }
}
