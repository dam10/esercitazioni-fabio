package it.corsojava.ex01;

/**
 * @author Fabio Napoleoni
 */
public class Fattorizzazione {
    public static void main(String[] args) {
        fattorizzazione(7);
        fattorizzazione(3000);
        fattorizzazione(10000000000001L);
    }

    /**
     * La scomposizione in fattori primi di un numero n è l'elenco dei fattori primi di n con le loro molteplicità.
     * Un fattore primo di n è un divisore di n che è un numero primo (un numero è primo se non ha divisori propri).
     * Implementare nel seguente metodo un algoritmo che stampa una stringa che descrive la fattorizzazione di n.
     * Ad esempio, con i seguenti input tornerà i seguenti output
     * 7 => "7"
     * 3000 => "2(3) 3 5(3)"
     * 10000000000001 => "11 859 1058313049"
     *
     * Nell'esempio con 3000 i numeri tra parentesi rappresentano l'esponente con cui il fattore
     * compare nella fattorizzazione, ovvero 2^3 * 3 * 5^3
     */
    public static void fattorizzazione(long n) {
        // Completare qui il programma usando la variabile in input n
    }
}
