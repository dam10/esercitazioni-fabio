package it.corsojava.ex01.Boni;

/**
 * @author Fabio Napoleoni
 */
public class MediaAritmetica {

    public static void main(String[] args) {
        System.out.println(mediaNumeriDispari(1, 100));
        System.out.println(mediaNumeriDispari(20, 1000));
    }

    /**
     * Completare la funzione in modo tale che ritorni la media aritmetica dei numeri dispari da i a n (inclusi)
     * @param i valore iniziale
     * @param n valore limite
     * @return la media aritmetica dei numeri nell'intervallo richiesto
     */
    static double mediaNumeriDispari(int i, int n) {
        //dichiaro la variabile per la somma dei numeri idonei
        double sommaNumeri = 0;
        double mediaNumeri;
        int b=1;
        int contatore = 0;
        for (int a = i; a <= n; a++) {
            if (a % 2 != 0) {
                sommaNumeri += a;
                contatore++;
            }
        }

        mediaNumeri = sommaNumeri / contatore;
        System.out.print("la media dei numeri da " + i + " a " + n + " è ==>  ");
        return mediaNumeri;
    }
}
