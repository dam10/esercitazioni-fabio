package it.corsojava.ex01.Boni;

/**
 * @author Fabio Napoleoni
 */
public class Monete {
    public static void main(String[] args) {
        monete(97);
    }

    /**
     * Scrivere un programma che letto un numero intero rappresentante un importo in centesimi di euro stampa
     * le monete da 50, 20, 10, 5, 2 e 1 centesimi di euro che servono per fare l'importo.
     * Ad esempio, se l'importo è di 97 centesimi allora il programma stampa:
     *
     * 1 moneta da 50
     * 2 monete da 20
     * 1 moneta da 5
     * 1 moneta da 2
     *
     * @param n il numero di centesimi da stampare
     */
    public static void monete(int n) {
        // Completare qui il programma usando la variabile in input n
        int contatore = 0;
        int a = 50;
        int b = 20;
        int d = 5;
        int e = 2;

        System.out.println("Importo-->  " + n);
        while (n >= 50) {
            ++contatore;
            n -= a;
        }
        if (contatore != 0) {
            System.out.println(contatore + " monete da 50");
        }
        contatore = 0;
        while (n >= 20) {
            contatore++;
            n -= b;
        }
        if (contatore != 0) {
            System.out.println(contatore + " monete da 20");
        }
        contatore = 0;
        while (n >= 5) {
            ++contatore;
            n -= d;
        }
        if (contatore != 0) {
            System.out.println(contatore + " monete da 5");
        }
        contatore = 0;
        while (n >= 2) {
            ++contatore;
            n -= e;
        }
        if (contatore != 0) {
            System.out.println(contatore + " monete da 2");
        }
    }
}
