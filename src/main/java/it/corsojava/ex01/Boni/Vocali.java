package it.corsojava.ex01.Boni;

import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class Vocali {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String line;
        while ((line = in.nextLine()) != null) {
            vocali(line);
        }
    }

    /**
     * Scrivere un programma che legge una linea di testo e per ogni vocale stampa il numero di volte che appare nella
     * linea di testo. Ad esempio, se la linea di testo è "mi illumino di immenso" allora il programma stampa:
     * <p>
     * a: 0    e: 1    i: 5    o: 2    u: 1
     * <p>
     * N.B. non si possono usare array per questo esercizio
     *
     * @param line input line
     */
    public static void vocali(String line) {
        //charAt
        //switch
        //count
        int count_a = 0;
        int count_e = 0;
        int count_i = 0;
        int count_o = 0;
        int count_u = 0;

        for (char c : line.toCharArray()) {
            switch (c) {
                case 'a':
                    count_a++;
                    break;
                case 'e':
                    count_e++;
                    break;
                case 'i':
                    count_i++;
                    break;
                case 'o':
                    count_o++;
                    break;
                case 'u':
                    count_u++;
                    break;
                default:
                    break;
            }
        }

        System.out.print("a: " + count_a + "    e: " + count_e + "    i: " + count_i + "    o: "
                + count_o + "    u: " + count_u);
    }
}
