package it.corsojava.ex01.Boni;

/**
 * @author Fabio Napoleoni
 */
public class CifreLettere {
    public static void main(String[] args) {
        cifreToLettere(2127);
        cifreToLettere(3621);
    }

    /**
     * Scrivere un programma che legge un intero n e stampa le cifre di n in lettere.
     * Ad esempio, se n = 2127, il programma stampa esattamente: due uno due sette.
     * @param n il numero da scomporre in cifre
     */
    public static void cifreToLettere(int n) {
        // Completare qui il programma usando la variabile in input n
        //creo un array e gli do valore n
        String numero = n + "";
        String[] numArray = new String[numero.length()];
        for (int i = 0; i < numero.length(); i++) {
            numArray[i] = numero.substring(i, i + 1);
        }
        for (String s : numArray) {
            String h;
            switch (s) {
                case "1":
                    h = "uno";
                    break;
                case "2":
                    h = "due";
                    break;
                case "3":
                    h = "tre";
                    break;
                case "4":
                    h = "quattro";
                    break;
                case "5":
                    h = "cinque";
                    break;
                case "6":
                    h = "sei";
                    break;
                case "7":
                    h = "sette";
                    break;
                case "8":
                    h = "otto";
                    break;
                case "9":
                    h = "nove";
                    break;
                case "0":
                    h = "zero";
                    break;
                default:
                    h = "-";
            }
            System.out.print(h + " ");
        }
        System.out.println();
    }
}
