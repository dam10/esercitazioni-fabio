package it.corsojava.ex01.Boni;

/**
 * @author Fabio Napoleoni
 */
public class ProduttoriaNumeri {

    public static void main(String[] args) {
        System.out.println(produttoriaNumeri(1, 20));
    }

    /**
     * Ritorna la produttoria dei numeri multipli di 3 nel range dato
     * @param inizio inizio intervallo (incluso)
     * @param fine fine intervallo (incluso)
     * @return il valore del calcolo
     */
    public static long produttoriaNumeri(int inizio, int fine) {
        long produttoria = 1;
        for (int a = inizio; a <= fine; a++) {
            if (a % 3 == 0) {
                produttoria *= a;
            }
        }

        return produttoria;
    }
}
