package it.corsojava.ex01;

import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class Vocali {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String line;
        while ((line = in.nextLine()) != null) {
            vocali(line);
        }
    }

    /**
     * Scrivere un programma che legge una linea di testo e per ogni vocale stampa il numero di volte che appare nella
     * linea di testo. Ad esempio, se la linea di testo è "mi illumino di immenso" allora il programma stampa:
     *
     * a: 0    e: 1    i: 5    o: 2    u: 1
     *
     * N.B. non si possono usare array per questo esercizio
     *
     * @param line input line
     */
    static void vocali(String line) {
    }
}
