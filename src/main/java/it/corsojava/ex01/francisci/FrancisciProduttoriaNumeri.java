package it.corsojava.ex01.francisci;

/**
 * @author Fabio Napoleoni
 */
public class FrancisciProduttoriaNumeri {

    public static void main(String[] args) {
        System.out.println(produttoriaNumeri(1, 20));
    }

    /**
     * Ritorna la produttoria dei numeri multipli di 3 nel range dato
     * @param inizio inizio intervallo (incluso)
     * @param fine fine intervallo (incluso)
     * @return il valore del calcolo
     */
    public static long produttoriaNumeri(int inizio, int fine) {
        int risultato = 1;
        for (int i = inizio; i<fine;i++){
            if(i % 3 == 0){
                risultato *= i;
            }
        }
        System.out.println(risultato);
        return risultato;
    }
}
