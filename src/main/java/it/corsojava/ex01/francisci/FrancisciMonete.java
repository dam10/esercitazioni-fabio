package it.corsojava.ex01.francisci;

/**
 * @author Marco Francisci
 */
public class FrancisciMonete {
    public static void main(String[] args) {
        monete(97);
    }

    /**
     * Scrivere un programma che letto un numero intero rappresentante un importo in centesimi di euro stampa
     * le monete da 50, 20, 10, 5, 2 e 1 centesimi di euro che servono per fare l'importo.
     * Ad esempio, se l'importo è di 97 centesimi allora il programma stampa:
     *
     * 1 moneta da 50
     * 2 monete da 20
     * 1 moneta da 5
     * 1 moneta da 2
     *
     * @param n il numero di centesimi da stampare
     */
    public static void monete(int n) {
        // Completare qui il programma usando la variabile in input n
        int cinquanta;
        int venti;
        int dieci;
        int cinque;
        int due;

        int resto;

        if( n >= 50 ){
            cinquanta = n/50;
            n = n % 50;
            System.out.print(cinquanta + " moneta da 50\n");
        }

        if(n>=20){
            venti = n/20;
            n = n % 20;
            System.out.print(venti + " monete da 20\n");
        }

        if(n>=10){
            dieci = n/10;
            n = n % 10;
            System.out.print(dieci + " moneta da 10\n");
        }

        if(n>=5){
            cinque = n/5;
            n = n % 5;
            System.out.print(cinque + " moneta da 5\n");
        }

        if(n>=2){
            due = n / 2;
            n = n % 2;
            System.out.print(due + " moneta da 2\n");
        }

        if(n==1){
            System.out.print(n + " moneta da 1\n");
        }
    }
}
