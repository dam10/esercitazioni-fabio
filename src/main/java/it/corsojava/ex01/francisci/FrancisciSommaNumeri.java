package it.corsojava.ex01.francisci;

/**
 * @author Marco Francisci
 */
public class FrancisciSommaNumeri {

    public static void main(String[] args) {
        System.out.println(sommaDaUnoAN(100));
        System.out.println(sommaDaUnoAN(1000));
        System.out.println(sommaDaUnoAN(10000));
    }

    /**
     * Ritorna la somma dei numeri naturali da 1 a n (incluso)
     *
     * @param n il numero limite
     * @return la somma calcolata come long
     */
    public static long sommaDaUnoAN(int n) {
        long somma = 0;
        for (long i=1; i <= n; i++){
            somma += i;
        }
        return somma;
    }
}
