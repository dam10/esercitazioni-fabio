package it.corsojava.ex01.francisci;

/**
 * @author Marco Francisci
 */
public class FrancisciCifreLettere {
    public static void main(String[] args) {
        cifreToLettere(2127);
        cifreToLettere(36021);
    }

    /**
     * Scrivere un programma che legge un intero n e stampa le cifre di n in lettere.
     * Ad esempio, se n = 2127, il programma stampa esattamente: due uno due sette.
     *
     * @param n il numero da scomporre in cifre
     */
    public static void cifreToLettere(int n) {
        // Completare qui il programma usando la variabile in input n

        String[] numeriInLettere = {"zero", "uno", "due", "tre", "quattro", "cinque", "sei", "sette", "otto", "nove"};
        String risultato = "";
        int[] numeri = new int[15];
        int i = 1;
        int v = 0;
        int b = 0;
        int x = 0;

        if (n < 10) {
            for (int c = 0; c < 10; c++) {
                if (n == c) {
                    risultato = numeriInLettere[c];
                }
            }
        } else {

            i = 10;
            while (n % i != n) {

                b = n % i;
                if (b < 10) {
                    x = b;
                } else {
                    x = (b - numeri[v]) / (i / 10);
                }
                numeri[v] = b;
                i *= 10;
                v += 1;
                for (int c = 0; c < 10; c++) {
                    if (x == c) {
                        if (risultato == "") {
                            risultato = numeriInLettere[c];
                        } else {
                            risultato = numeriInLettere[c] + " " + risultato;
                        }

                    }
                }
            }

            x = (n - b) / (i / 10);

            for (int c = 0; c < 10; c++) {
                if (x == c) {
                    if (risultato == "") {
                        risultato = numeriInLettere[c];
                    } else {
                        risultato = numeriInLettere[c] + " " + risultato;
                    }
                }
            }
        }
        System.out.print(risultato + "\n");
    }

}
