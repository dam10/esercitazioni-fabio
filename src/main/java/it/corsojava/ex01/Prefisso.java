package it.corsojava.ex01;

import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class Prefisso {

    /**
     * Scrivere un metodo che prende in input due stringhe e ritorna la lunghezza del più lungo prefisso comune alle
     * due stringhe. Ad esempio, se le due stringhe di input sono "Le cose non sono solo cose" e
     * "Le cose sono solo cose", il programma stampa 8.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = in.nextLine();
        String s2 = in.nextLine();
        // ...
    }
}
