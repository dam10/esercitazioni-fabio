package it.corsojava.ex01;

import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class StringheVerticali {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Leggo le prime 3 righe di input
        String linea1 = in.nextLine(),
                linea2 = in.nextLine(),
                linea3 = in.nextLine();
        stampaVerticale(linea1, linea2, linea3);
    }

    /**
     * Scrivere un programma che legge tre stringhe e le stampa in verticale l'una accanto all'altra.
     * Ad esempio, se le stringhe sono "gioco", "OCA" e "casa" allora il programma stampa:
     *
     * gOc
     * iCa
     * oAs
     * c a
     * o
     * 
     * @param linea1 prima riga
     * @param linea2 seconda riga
     * @param linea3 terza riga
     */
    static void stampaVerticale(String linea1, String linea2, String linea3) {
    }
}
