package it.corsojava.ex01.amadei;

/**
 * @author Fabio Napoleoni
 */
public class Monete {
    public static void main(String[] args) {
        monete(97);
    }

    /**
     * Scrivere un programma che letto un numero intero rappresentante un importo in centesimi di euro stampa
     * le monete da 50, 20, 10, 5, 2 e 1 centesimi di euro che servono per fare l'importo.
     * Ad esempio, se l'importo è di 97 centesimi allora il programma stampa:
     *
     * 1 moneta da 50
     * 2 monete da 20
     * 1 moneta da 5
     * 1 moneta da 2
     *
     * @param n il numero di centesimi da stampare
     */
    public static void monete(int n) {
        int contatore50= 0;
        int contatore20 = 0;
        int contatore5= 0;
        int contatore2= 0;

        while (n != 0) {
            if (n >= 50) {
                n = n -50;
                contatore50++;
            } else if (n >= 20) {
                n = n - 20;
                contatore20++;
            } else if (n >= 5) {
                n = n - 5;
                contatore5++;
            } else if (n >= 2) {
                n = n - 2;
                contatore2++;
            }
        }

        System.out.print(contatore50 + " moneta da 50" + "\n");
        System.out.print(contatore20 + " monete da 20"+ "\n");
        System.out.print(contatore5 + " moneta da 5"+ "\n");
        System.out.print(contatore2 + " moneta da 2"+ "\n");
    }
}
