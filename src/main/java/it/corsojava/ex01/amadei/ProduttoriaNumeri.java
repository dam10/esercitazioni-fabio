package it.corsojava.ex01.amadei;

/**
 * @author Fabio Napoleoni
 */
public class ProduttoriaNumeri {

    public static void main(String[] args) {
        System.out.println(produttoriaNumeri(1, 20));
    }

    /**
     * Ritorna la produttoria dei numeri multipli di 3 nel range dato
     * @param inizio inizio intervallo (incluso)
     * @param fine fine intervallo (incluso)
     * @return il valore del calcolo
     */
    public static long produttoriaNumeri(int inizio, int fine) {
        long prodottoNumeri=1;

        for (int i=inizio; i<=fine; i++){
            if (i%3 !=0){
                continue;
            } else {
                prodottoNumeri = prodottoNumeri * i;
            }

        }

        return prodottoNumeri;
    }
}
