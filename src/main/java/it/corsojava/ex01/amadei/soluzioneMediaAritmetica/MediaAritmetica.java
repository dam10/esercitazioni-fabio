package it.corsojava.ex01.amadei.soluzioneMediaAritmetica;

/**
 * @author Fabio Napoleoni
 */
public class MediaAritmetica {

    public static void main(String[] args) {
        System.out.println(mediaNumeriDispari(1, 100));
        System.out.println(mediaNumeriDispari(20, 1000));
    }

    /**
     * Completare la funzione in modo tale che ritorni la media aritmetica dei numeri dispari da i a n (inclusi)
     * @param i valore iniziale
     * @param n valore limite
     * @return la media aritmetica dei numeri nell'intervallo richiesto
     */
    static double mediaNumeriDispari(int i, int n) {
        int sommaNumeri = n * (n + i) / 2;
        int numeroNumeri = n - i;

        double mediaNumeriDispari = sommaNumeri / numeroNumeri;

        return mediaNumeriDispari;
    }
}
