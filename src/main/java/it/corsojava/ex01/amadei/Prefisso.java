package it.corsojava.ex01.amadei;

import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class Prefisso {

    /**
     * Scrivere un metodo che prende in input due stringhe e ritorna la lunghezza del più lungo prefisso comune alle
     * due stringhe. Ad esempio, se le due stringhe di input sono "Le cose non sono solo cose" e
     * "Le cose sono solo cose", il programma stampa 8.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = in.nextLine();
        String s2 = in.nextLine();
        // ...
        int lunghezzaMinima;
        if (s1.length()< s2.length()){
            lunghezzaMinima = s1.length();

        }else {
            lunghezzaMinima = s2.length();
        }

        int prefisso = 0;
        for (int i= 0; i<lunghezzaMinima; i++){

            if (s1.charAt(i)==s2.charAt(i)){
                prefisso ++;

            } else {
                break;
            }

        }

        System.out.print(prefisso + "\n");
    }
}
