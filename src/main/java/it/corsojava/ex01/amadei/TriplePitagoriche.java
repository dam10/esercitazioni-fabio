package it.corsojava.ex01.amadei;

/**
 * @author Fabio Napoleoni
 */
public class TriplePitagoriche {
    public static void main(String[] args) {
        triplePitagoriche(26);
    }

    /**
     * Una tripla pitagorica è una tripla di numeri interi a, b, c tali che 1 ≤ a ≤ b ≤ c e a^2 + b^2 = c^2.
     * Ciò equivale a dire che a, b, c sono le misure dei lati di un triangolo rettangolo (da qui il nome).
     * Scrivere un programmma che legge un intero M e stampa tutte le triple pitagoriche con c ≤ M.
     * <p>
     * Ad esempio con l'input 26 il programma produrrà l'output seguente
     * <p>
     * (3, 4, 5)
     * (5, 12, 13)
     * (7, 24, 25)
     *
     * @param M il numero limite per cui trovare le triple pitagoriche
     */
    public static void triplePitagoriche(int M) {
        // Completare qui il programma usando la variabile in input M
        for (int i = 1; i <= M; i++) {
            int b = 1;
            int c = 1;

            for (b = 1; b < M; b++) {
                if ((((i * i) + (b * b)) == (c * c)) && ((c < M) && (i < M))) {

                    System.out.print("(" + i + ", " + b + ", " + c + ")"+"\n");
                }

                for (c = 1; c < M; c++)
                    if ((((i * i) + (b * b)) == (c * c)) && ((b < M) && (i < M)) && (i < b))
                        System.out.print("(" + i + ", " + b + ", " + c + ")"+"\n");
            }
        }
    }
}