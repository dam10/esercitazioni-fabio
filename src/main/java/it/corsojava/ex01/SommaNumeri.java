package it.corsojava.ex01;

/**
 * @author Fabio Napoleoni
 */
public class SommaNumeri {

    public static void main(String[] args) {
        System.out.println(sommaDaUnoAN(100));
        System.out.println(sommaDaUnoAN(1000));
        System.out.println(sommaDaUnoAN(10000));
    }

    // 1   2
    // 100 99

    // 101 101 .... per 100 colonne
    // 101 * 100 / 2 = 5050

    // da 1 a 10
    //  1  2  3  4  5  6  7  8  9 10   +
    // 10  9  8  7  6  5  4  3  2  1   =

    // 11 11 11 11 11 11 11 11 11 11

    // 10 * 11 = 110 / 2

    // da 1 a n
    // n colonne la cui somma fa (n + 1)

    // la riposta al mio problema n * (n + 1) / 2


    /**
     * Ritorna la somma dei numeri naturali da 1 a n (incluso)
     *
     * @param n il numero limite
     * @return la somma calcolata come long
     */
    public static long sommaDaUnoAN(int n) {
        // Leggi n come se fossse un long
        return (long) n * ((long) n + 1) / 2;
    }
}
