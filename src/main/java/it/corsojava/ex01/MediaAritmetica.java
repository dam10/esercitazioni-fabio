package it.corsojava.ex01;

/**
 * @author Fabio Napoleoni
 */
public class MediaAritmetica {

    public static void main(String[] args) {
        System.out.println(mediaNumeriDispari(1, 100));
        System.out.println(mediaNumeriDispari(20, 1000));
    }

    /**
     * Completare la funzione in modo tale che ritorni la media aritmetica dei numeri dispari da i a n (inclusi)
     * @param i valore iniziale
     * @param n valore limite
     * @return la media aritmetica dei numeri nell'intervallo richiesto
     */
    public static double mediaNumeriDispari(int i, int n) {
        // somma da 1 a n = n * (n + 1) / 2
        double sum = 0.0;
        int count = 0;
        for (int m = i; m <= n; m++) {
            if (m % 2 == 0) continue; // salto i pari
            sum += m; // aggiungo il numero corrente alla somma
            count++;
        }
        // qui sum e' la somma di tutti i dispari
        // count e' il numero di tutti i dispari
        return sum / count;
    }
}
