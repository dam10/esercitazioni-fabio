package it.corsojava.ex01.bicini;

/**
 * @author Fabio Napoleoni
 */
public class SoluzioneCifreLettere {
	public static void main(String[] args) {
		cifreToLettere(2127);
		cifreToLettere(3621);
		cifreToLettere(89709721);
	}

	/**
	 * Scrivere un programma che legge un intero n e stampa le cifre di n in
	 * lettere. Ad esempio, se n = 2127, il programma stampa esattamente: due uno
	 * due sette.
	 * 
	 * @param n il numero da scomporre in cifre
	 */
	public static void cifreToLettere(int n) {
		//soluzione cifreLettere
		int temp = 0;
		while (n != 0) {
			temp = temp * 10 + n % 10;
			n /= 10;
		}

		while (temp != 0) {
			n = temp % 10;
			switch (n) {
			case 1:
				System.out.print("uno ");
				break;
			case 2:
				System.out.print("due ");
				break;
			case 3:
				System.out.print("tre ");
				break;
			case 4:
				System.out.print("quattro ");
				break;
			case 5:
				System.out.print("cinque ");
				break;
			case 6:
				System.out.print("sei ");
				break;
			case 7:
				System.out.print("sette ");
				break;
			case 8:
				System.out.print("otto ");
				break;
			case 9:
				System.out.print("nove ");
				break;
			case 0:
				System.out.print("zero ");
				break;
			}
			temp /= 10;
		}
		System.out.println();
	}
}
