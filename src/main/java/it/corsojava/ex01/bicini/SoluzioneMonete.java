package it.corsojava.ex01.bicini;

/**
 * @author Fabio Napoleoni
 */
public class SoluzioneMonete {
	public static void main(String[] args) {
		monete(97);
	}

	/**
	 * Scrivere un programma che letto un numero intero rappresentante un importo in
	 * centesimi di euro stampa le monete da 50, 20, 10, 5, 2 e 1 centesimi di euro
	 * che servono per fare l'importo. Ad esempio, se l'importo è di 97 centesimi
	 * allora il programma stampa:
	 *
	 * 1 moneta da 50 2 monete da 20 1 moneta da 5 1 moneta da 2
	 *
	 * @param n il numero di centesimi da stampare
	 */
	public static void monete(int n) {
		// Completare qui il programma usando la variabile in input n
		int cinquantaC, ventiC, dieciC, cinqueC, dueC, unC;
		if (n <= 0)
			System.out.println("errore: inserire un imorto valido.");
		else {

			cinquantaC = n / 50;
			n = n % 50;
			if (cinquantaC == 1)
				System.out.println(cinquantaC + " moneta da 50 centesimi.");
			else
				System.out.println(cinquantaC + " monete da 50 centesimi.");
			ventiC = n / 20;
			n = n % 20;
			if (ventiC == 1)
				System.out.println(ventiC + " moneta da 20 centesimi.");
			else
				System.out.println(ventiC + " monete da 20 centesimi.");
			dieciC = n / 10;
			n = n % 10;
			if (dieciC == 1)
				System.out.println(dieciC + " moneta da 10 centesimi.");
			else
				System.out.println(dieciC + " monete da 10 centesimi.");
			cinqueC = n / 5;
			n = n % 5;
			if (cinqueC == 1)
				System.out.println(cinqueC + " moneta da 5 centesimi.");
			else
				System.out.println(cinqueC + " monete da 5 centesimi.");
			dueC = n / 2;
			n = n % 2;
			if (dueC == 1)
				System.out.println(dueC + " moneta da 2 centesimi.");
			else
				System.out.println(dueC + " monete da 2 centesimi.");
			unC = n;
			if (unC == 1)
				System.out.println(unC + " moneta da 1 centesimo.");
			else
				System.out.println(unC + " monete da 1 centesimo.");
		}
	}
}
