package it.corsojava.ex01;

/**
 * @author Fabio Napoleoni
 */
public class ProduttoriaNumeri {

    public static void main(String[] args) {
        System.out.println(produttoriaNumeri(1, 20));
    }

    /**
     * Ritorna la produttoria dei numeri multipli di 3 nel range dato
     * @param inizio inizio intervallo (incluso)
     * @param fine fine intervallo (incluso)
     * @return il valore del calcolo
     */
    public static long produttoriaNumeri(int inizio, int fine) {
        int produttoria = 1;
        for(;inizio <= fine;inizio++){
            if((inizio % 3) == 0){
                produttoria = produttoria * inizio;
            }
        }
        return produttoria;
    }
}
