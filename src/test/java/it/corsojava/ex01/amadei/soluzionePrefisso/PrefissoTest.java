package it.corsojava.ex01.amadei.soluzionePrefisso;

import it.corsojava.ex01.amadei.Prefisso;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

/**
 * @author Fabio Napoleoni
 */

public class PrefissoTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

    @Test
    public void test() {
        systemInMock.provideLines("Le cose non sono solo cose", "Le cose sono solo cose");
        Prefisso.main(new String[]{});
        assertThat(systemOutRule.getLog()).isEqualTo("8\n");
    }
}
