package it.corsojava.ex01.amadei.SoluzioneProduttoria;

import it.corsojava.ex01.amadei.ProduttoriaNumeri;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */

public class ProduttoriaTest {

    @Test
    public void testProduttoria() {
        assertThat(ProduttoriaNumeri.produttoriaNumeri(1, 20)).isEqualTo(524880);
    }

    @Test
    public void testConZero() {
        assertThat(ProduttoriaNumeri.produttoriaNumeri(0, 100)).isEqualTo(0);
    }
}
