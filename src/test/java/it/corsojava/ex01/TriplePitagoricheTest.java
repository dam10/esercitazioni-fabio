package it.corsojava.ex01;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class TriplePitagoricheTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test() {
        TriplePitagoriche.triplePitagoriche(26);
        assertThat(systemOutRule.getLog()).isEqualTo("(3, 4, 5)\n" +
                "(5, 12, 13)\n" +
                "(7, 24, 25)\n"
        );
    }
}
