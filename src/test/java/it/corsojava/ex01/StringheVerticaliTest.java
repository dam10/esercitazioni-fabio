package it.corsojava.ex01;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class StringheVerticaliTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void testStampaVerticale() {
        // Method under test
        StringheVerticali.stampaVerticale("gioco", "OCA", "casa");
        // check
        assertThat(systemOutRule.getLog()).isEqualTo(
                "gOc\niCa\n" +
                        "oAs\n" + "c a\n" +
                        "o  \n"
        );
    }
}
