package it.corsojava.ex01.BoniTest;

import it.corsojava.ex01.Boni.Vocali;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
public class VocaliTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void testStampaVerticale() {
        // Method under test
        Vocali.vocali("mi illumino di immenso");
        // check
        assertThat(systemOutRule.getLog()).isEqualTo(
                "a: 0    e: 1    i: 5    o: 2    u: 1"
        );
    }
}
