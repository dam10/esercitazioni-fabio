package it.corsojava.ex01.francisci;

import it.corsojava.ex01.francisci.FrancisciProduttoriaNumeri;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
public class FrancisciProduttoriaTest {

    @Test
    public void testProduttoria() {
        assertThat(FrancisciProduttoriaNumeri.produttoriaNumeri(1, 20)).isEqualTo(524880);
    }

    @Test
    public void testConZero() {
        assertThat(FrancisciProduttoriaNumeri.produttoriaNumeri(0, 100)).isEqualTo(0);
    }
}
