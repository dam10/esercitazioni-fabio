package it.corsojava.ex01.francisci;

import it.corsojava.ex01.francisci.FrancisciCifreLettere;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
public class FrancisciCifreLettereTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test2127() {
        FrancisciCifreLettere.cifreToLettere(2127);
        assertThat(systemOutRule.getLog()).isEqualTo("due uno due sette\n");
    }

    @Test
    public void test127() {
        FrancisciCifreLettere.cifreToLettere(36021);
        assertThat(systemOutRule.getLog()).isEqualTo("tre sei zero due uno\n");
    }

}
