package it.corsojava.ex01.francisci;

import it.corsojava.ex01.francisci.FrancisciSommaNumeri;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Marco Francisci
 */
public class FrancisciSommaNumeriTest {

    @Test
    public void testSommaGenerale() {
        assertThat(FrancisciSommaNumeri.sommaDaUnoAN(3)).isEqualTo(6);
        assertThat(FrancisciSommaNumeri.sommaDaUnoAN(100)).isEqualTo(5050);
        assertThat(FrancisciSommaNumeri.sommaDaUnoAN(10000)).isEqualTo(50005000);
    }

    @Test
    public void testSommaConZero() {
        assertThat(FrancisciSommaNumeri.sommaDaUnoAN(0)).isEqualTo(0);
    }

    @Test(timeout = 2000)
    public void testSommaConValoreMassimo() {
        long expected = 2305843008139952128L;
        assertThat(FrancisciSommaNumeri.sommaDaUnoAN(Integer.MAX_VALUE)).isEqualTo(expected);
    }

//    @Test(expected = IllegalArgumentException.class)
//    @Ignore
//    public void testSommaEccezione() {
//        SommaNumeri.sommaDaUnoAN(-1);
//    }
    
}
