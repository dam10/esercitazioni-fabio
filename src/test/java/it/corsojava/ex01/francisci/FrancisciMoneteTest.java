package it.corsojava.ex01.francisci;

import it.corsojava.ex01.francisci.FrancisciMonete;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Marco Francisci
 */
public class FrancisciMoneteTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test() {
        FrancisciMonete.monete(97);
        assertThat(systemOutRule.getLog()).isEqualTo(
                "1 moneta da 50\n" +
                        "2 monete da 20\n" +
                        "1 moneta da 5\n" +
                        "1 moneta da 2\n"
        );
    }

}
