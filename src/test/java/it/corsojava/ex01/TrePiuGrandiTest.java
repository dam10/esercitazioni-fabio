package it.corsojava.ex01;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class TrePiuGrandiTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

    @Test
    public void test() {
        systemInMock.provideLines("2 10 8 7 1 12 2 -1");
        TrePiuGrandi.main(new String[]{});
        assertThat(systemOutRule.getLog()).isEqualTo(
                "I tre numeri più grandi sono: 12, 10, 8\n"
        );
    }
}
