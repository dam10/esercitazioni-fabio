package it.corsojava.ex01.villa.soluzioneMonete;

import it.corsojava.ex01.Monete;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class MoneteTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test() {
        Monete.monete(97);
        assertThat(systemOutRule.getLog()).isEqualTo(
                "1 moneta da 50\r\n" +
                        "2 monete da 20\r\n" +
                        "1 moneta da 5\r\n" +
                        "1 moneta da 2\r\n"
        );
    }

}
