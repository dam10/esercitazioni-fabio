package it.corsojava.ex01.villa.soluzioneSommaNumeri;

import it.corsojava.ex01.SommaNumeri;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class SommaNumeriTest {

    @Test
    public void testSommaGenerale() {
        assertThat(it.corsojava.ex01.SommaNumeri.sommaDaUnoAN(3)).isEqualTo(6);
        assertThat(it.corsojava.ex01.SommaNumeri.sommaDaUnoAN(100)).isEqualTo(5050);
        assertThat(it.corsojava.ex01.SommaNumeri.sommaDaUnoAN(10000)).isEqualTo(50005000);
    }

    @Test
    public void testSommaConZero() {
        assertThat(it.corsojava.ex01.SommaNumeri.sommaDaUnoAN(0)).isEqualTo(0);
    }

    @Test(timeout = 2000)
    public void testSommaConValoreMassimo() {
        long expected = 2305843008139952128L;
        assertThat(SommaNumeri.sommaDaUnoAN(Integer.MAX_VALUE)).isEqualTo(expected);
    }

//    @Test(expected = IllegalArgumentException.class)
//    @Ignore
//    public void testSommaEccezione() {
//        SommaNumeri.sommaDaUnoAN(-1);
//    }
    
}
