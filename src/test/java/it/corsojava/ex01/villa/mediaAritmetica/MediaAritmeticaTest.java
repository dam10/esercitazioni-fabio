package it.corsojava.ex01.villa.mediaAritmetica;

import it.corsojava.ex01.MediaAritmetica;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class MediaAritmeticaTest {

    @Test
    public void testMedia() {
        assertThat(it.corsojava.ex01.MediaAritmetica.mediaNumeriDispari(1, 3)).isEqualTo(2.0);
        assertThat(it.corsojava.ex01.MediaAritmetica.mediaNumeriDispari(1, 4)).isEqualTo(2.0);
        assertThat(it.corsojava.ex01.MediaAritmetica.mediaNumeriDispari(0, 100)).isEqualTo(50.0);
    }

    @Test
    public void testMediaDa20a1000() {
        assertThat(it.corsojava.ex01.MediaAritmetica.mediaNumeriDispari(20, 1000)).isEqualTo(510.0);
    }

    @Test
    public void testMediaSenzaInput() {
        assertThat(it.corsojava.ex01.MediaAritmetica.mediaNumeriDispari(1, 1)).isEqualTo(1);
        assertThat(it.corsojava.ex01.MediaAritmetica.mediaNumeriDispari(2, 2)).isNaN();
        assertThat(MediaAritmetica.mediaNumeriDispari(100, 0)).isNaN();
    }

}
