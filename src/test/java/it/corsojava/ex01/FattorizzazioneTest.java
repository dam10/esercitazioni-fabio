package it.corsojava.ex01;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class FattorizzazioneTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test7() {
        Fattorizzazione.fattorizzazione(7);
        assertThat(systemOutRule.getLog()).isEqualTo("7\n");
    }

    @Test
    public void test3000() {
        Fattorizzazione.fattorizzazione(3000);
        assertThat(systemOutRule.getLog()).isEqualTo("2(3) 3 5(3)\n");
    }

    @Test
    public void test10000000000001() {
        Fattorizzazione.fattorizzazione(10000000000001L);
        assertThat(systemOutRule.getLog()).isEqualTo("11 859 1058313049\n");
    }
}
