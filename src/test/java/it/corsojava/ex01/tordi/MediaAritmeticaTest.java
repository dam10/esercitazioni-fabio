package it.corsojava.ex01.tordi;

import it.corsojava.ex01.MediaAritmetica;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
public class MediaAritmeticaTest {

    @Test
    public void testMedia() {
        assertThat(MediaAritmetica.mediaNumeriDispari(1, 3)).isEqualTo(2.0);
        assertThat(MediaAritmetica.mediaNumeriDispari(1, 4)).isEqualTo(2.0);
        assertThat(MediaAritmetica.mediaNumeriDispari(0, 100)).isEqualTo(50.0);
    }

    @Test
    public void testMediaDa20a1000() {
        assertThat(MediaAritmetica.mediaNumeriDispari(20, 1000)).isEqualTo(510.0);
    }

    @Test
    public void testMediaSenzaInput() {
        assertThat(MediaAritmetica.mediaNumeriDispari(1, 1)).isEqualTo(1);
        assertThat(MediaAritmetica.mediaNumeriDispari(2, 2)).isNaN();
        assertThat(MediaAritmetica.mediaNumeriDispari(100, 0)).isNaN();
    }

}
