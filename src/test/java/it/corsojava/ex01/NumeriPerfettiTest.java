package it.corsojava.ex01;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class NumeriPerfettiTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test6() {
        NumeriPerfetti.numeriPerfetti(6);
        assertThat(systemOutRule.getLog()).isEqualTo("6 = 1 + 2 + 3\n");
    }

    @Test
    public void test1000() {
        NumeriPerfetti.numeriPerfetti(1000);
        assertThat(systemOutRule.getLog()).isEqualTo(
                "6 = 1 + 2 + 3\n" +
                        "28 = 1 + 2 + 4 + 7 + 14\n" +
                        "496 = 1 + 2 + 4 + 8 + 16 + 31 + 62 + 124 + 248\n"
        );
    }
}
