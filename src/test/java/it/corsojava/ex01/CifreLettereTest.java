package it.corsojava.ex01;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Fabio Napoleoni
 */
@Ignore("Da completare nei vostri branch")
public class CifreLettereTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void test2127() {
        CifreLettere.cifreToLettere(2127);
        assertThat(systemOutRule.getLog()).isEqualTo("due uno due sette\n");
    }

    @Test
    public void test127() {
        CifreLettere.cifreToLettere(36021);
        assertThat(systemOutRule.getLog()).isEqualTo("tre sei zero due uno\n");
    }

}
